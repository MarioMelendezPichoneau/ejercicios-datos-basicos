﻿using System;

namespace DatoBasicos
{
    class Program
    {
        static void Main(string[] args)
        {
            //1. preguntar al usuario su edad, que se guardara en un "byte". a continuacion, se debera que no
            // aparenta tantos años ( por ejemplo, "No aparentas 20 años").
            
            Console.WriteLine(" introduce su edad: ");
            int edad = Convert.ToByte(Console.ReadLine());

            Console.WriteLine($" No aprentas {edad} años");
            Console.Read();
            
            


            // 2. pedir al usuario dos numeros enteros largos("long") y mostrar su suma, resta y su producto
            /*
            Console.WriteLine(" ingrese un numero");
            long num1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(" ingrese un numero");
            long num2 = Convert.ToInt32(Console.ReadLine());

            long suma = num1 + num2;
            long res = num1 - num2;
            long pro = num1 * num2;

            Console.WriteLine($" la suma es: {suma} \n la resta es: {res} \n el producto es: {pro} ");
            Console.Read();
            
            */



            // 3. crear un programa que use tres variables x,y,z. sus valores iniciales seran 15, -10, 2.147.487.647
            // se debera incrementar tres veces el valor de esta variables. mostrar en pantalla el valor inicial de
            // variables y el valor final obtenido por el programa
            /*
            int v1 = 15;
            int v2 = -10;
            float v3= 2.147.487.647;



            Console.WriteLine($"valor inicial {v1} valor final {Math.Pow(v1, 3)} ");
            Console.WriteLine($" valor inicial {v2} valor final {Math.Pow(v2,3)} ");
            //Console.WriteLine($" valor inicial {v2} valor final {Math.Pow(v3, 3)} ");

            Console.Read();
            

            */




            // 4. cual seria el resultado de las siguiente operaciones? a=5; b=++a; c=a++; b=b*5; a=a*2;
            /*
            int a = 5, b= ++a, c= a++;
            Console.WriteLine("a={0}, b={1}, c={2}", a, b, c);
            a= a*2; b= b*5;
            Console.WriteLine($"finalmente a={a}, b={b}, c={c}");

            Console.Read();
            

            */



            // 5. cual seria el resultado de las siguiente operaciones? a=5; b=a+2; b-=3; c=-3; c*=2; ++c; a*=b;
            /*
            int a = 5, b =a+2, c = -3;

            Console.WriteLine($"a={a}, b={b}, c={c}");
            b -= 3; c *= 2;
            Console.WriteLine($"luego b={b}, c={c}");
            a*=b; ++c;
            Console.WriteLine($"Y finalmente a={a}, b={b}, c={c}");

            Console.Read();
            
            */



            // 6. calcular el area de un circulo, dado su radio (pi* radio al cuadrado).
            /*
            double area, radio;

            Console.WriteLine(" ingrese el radio ");
            radio = Convert.ToDouble(Console.ReadLine());

            area = Math.PI * Math.Pow(radio, 2);
            Console.WriteLine($" El area del circulo es: {area} ");

            Console.Read();
            
            */


            // 7. crear un programa que pida al usurio a una distancia (en metroos) y el tiempo necesario para recorrerla
            // (como tres numero: horas, minutos, segundos), y muestre la velocidad, en metros por segundo, en kilometros 
            // por horas y en millas por horas(pista: 1 milla = 1.609 metros).

            /*
            Console.WriteLine(" ingrese la distancia recorrida en metros");
            int metros = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(" ingrese la cantidad de horas en la que fue recorrida esa distancia");
            int minutos = Convert.ToInt32(Console.ReadLine());

            int segundos = (minutos * 60*60);

            int velocidad = metros / segundos;
            Console.WriteLine($"su velocidad es {velocidad}m/s ");

            double kilometros = (metros / 1000);
            Console.WriteLine($"kilometros {kilometros}km/h ");

            double millas = kilometros / 1.609;
            Console.WriteLine($"Millas {millas}m/h ");


            Console.Read();
            */

        }
    }
}
